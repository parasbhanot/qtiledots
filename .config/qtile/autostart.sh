#!/bin/bash

setxkbmap -option caps:super &
picom -b
redshift -O 3600 &
# connman-gtk &
/home/parasbhanot/.config/fehloop.sh &
nm-applet &
dxhd &
dunst &
xidlehook --not-when-fullscreen --not-when-audio --timer 600 'betterlockscreen -l dim' '' &
udiskie &
xrandr --dpi 100 &
flameshot &
blueman-applet &
