
# Modified by Paras Bhanot

import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401
from libqtile.log_utils import logger

mod = "mod4"
terminal = "kitty"

keys = [
    # Switch between windows
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to left"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus to left"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus to left"),

    # Moving out of range in Columns layout will create new column.
    Key([mod, "control"], "Left", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "control"], "Right", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "control"], "Up", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "control"], "Down", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod], "h", lazy.layout.grow(),
        desc="Grow window to the left"),
    Key([mod], "l", lazy.layout.shrink(),
        desc="Grow window to the right"),
    Key([mod], "n", lazy.layout.reset()),

    Key([mod], "v", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "backslash", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "c", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),

    # My custom bindings

    # launch rofi
    Key([mod], "space", lazy.spawn("rofi -show drun"), desc="Launch rofi"),

    # Volume control
    Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer --decrease 5")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer --increase 5")),
    Key([], "XF86AudioMute", lazy.spawn("pamixer --toggle-mute")),

    # Brightness Control
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +10%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-")),

    # Shutdown qtile
    Key([mod], "BackSpace", lazy.shutdown(), desc="Shutdown Qtile"),

    # set tunuar File Manager kebinding
    Key([mod], "d", lazy.spawn("thunar")),

    # launch screen lock
    Key([mod], "x", lazy.spawn("betterlockscreen -l dim")),

    # Launch terminator
    Key([mod], "p", lazy.spawn("terminator")),

    # Launch subilme
    Key([mod], "s", lazy.spawn("subl")),

    # Launch dmenu config edit script
    Key([mod], "r", lazy.spawn(
        "sh /home/parasbhanot/.config/dmenu_scripts/quickeditor.sh")),

    # Launch image viewer
    # Key([mod], "w", lazy.spawn("sxiv -t /home/parasbhanot/Selected_wallpapers/")),

    Key([mod], "w", lazy.spawn("urxvt -e calcurse")),

]

# Define Group names

group_names = [("TER", {'layout': 'monadtall'}),
               ("WEB", {'layout': 'monadtall'}),
               ("DEV", {'layout': 'monadtall'}),
               ("MUS", {'layout': 'monadtall'}),
               ("VID", {'layout': 'monadtall'}),
               ("DOC", {'layout': 'monadtall'}),
               ("VBX", {'layout': 'floating'}),
               ("SER", {'layout': 'monadtall'}),
               ("GFX", {'layout': 'floating'}),
               ("TOR", {'layout': 'monadtall'})
               ]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

# Go to group

keys.append(Key([mod], "parenleft", lazy.group["TER"].toscreen()))
keys.append(Key([mod], "parenright", lazy.group["WEB"].toscreen()))
keys.append(Key([mod], "braceright", lazy.group["DEV"].toscreen()))
keys.append(Key([mod], "plus", lazy.group["MUS"].toscreen()))
keys.append(Key([mod], "braceleft", lazy.group["VID"].toscreen()))
keys.append(Key([mod], "bracketright", lazy.group["DOC"].toscreen()))
keys.append(Key([mod], "bracketleft", lazy.group["VBX"].toscreen()))
keys.append(Key([mod], "exclam", lazy.group["SER"].toscreen()))
keys.append(Key([mod], "equal", lazy.group["GFX"].toscreen()))
keys.append(Key([mod], "asterisk", lazy.group["TOR"].toscreen()))

# Send Window to group

keys.append(Key([mod], "KP_End", lazy.window.togroup("TER")))
keys.append(Key([mod], "KP_Down", lazy.window.togroup("WEB")))
keys.append(Key([mod], "KP_Next", lazy.window.togroup("DEV")))
keys.append(Key([mod], "KP_Left", lazy.window.togroup("MUS")))
keys.append(Key([mod], "KP_Begin", lazy.window.togroup("VID")))
keys.append(Key([mod], "KP_Right", lazy.window.togroup("DOC")))
keys.append(Key([mod], "KP_Home", lazy.window.togroup("VBX")))
keys.append(Key([mod], "KP_Up", lazy.window.togroup("SER")))
keys.append(Key([mod], "KP_Prior", lazy.window.togroup("GFX")))
keys.append(Key([mod], "KP_Insert", lazy.window.togroup("TOR")))

layout_theme = {"border_width": 4,
                "margin": 8,
                "border_focus": "#d79921",
                "border_normal": "#1D2330"
                }
layouts = [
    # layout.Max(),
    # layout.Matrix(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Tile(**layout_theme),
    # layout.Floating(
    #     border_focus='#d79921',
    #     border_width=2
    # ),
    layout.TreeTab(
        font="Ubuntu",
        fontsize=10,
        sections=["FIRST", "SECOND", "THIRD", "FOURTH"],
        section_fontsize=10,
        border_width=2,
        bg_color="#1c1f24",
        active_bg="#d75F5F",
        active_fg="#000000",
        inactive_bg="#a89984",
        inactive_fg="#1c1f24",
        padding_left=0,
        padding_x=0,
        padding_y=5,
        section_top=10,
        section_bottom=20,
        level_shift=8,
        vspace=3,
        panel_width=200
    ),
    # layout.VerticalTile(**layout_theme),
]

# Color Shchemes

soft_background = ["#1d2021", "#1d2021"]
hard_background = ["#1d2021", "#111313"]
font_color = ["#ebdbb2", "#ebdbb2"]

colors = [["#111313", "#111313"],  # panel background paras
          ["#3d3f4b", "#434758"],  # background for current screen tab
          ["#ebdbb2", "#ebdbb2"],  # font color for group names paras
          ["#ff5555", "#ff5555"],  # border line color for current tab

          # border line color for 'other tabs' and color for 'odd widgets' paras
          ["#a89984", "#a89984"],
          ["#504945", "#504945"],  # color for the 'even widgets' paras
          ["#458588", "#458588"],  # window name paras
          ["#98971a", "#98971a"]]  # backbround for inactive screens paras

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Font Awesome 5 Free Bold",

    #font="Ubuntu Mono Bold ",
    fontsize=12,
    padding=-2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
        widget.Sep(
            linewidth=0,
            padding=6,
            foreground=colors[2],
            background=colors[0]
        ),
        widget.GroupBox(
            font="Ubuntu Bold",
            fontsize=12,
            margin_y=3,
            margin_x=0,
            padding_y=5,
            padding_x=3,
            borderwidth=3,
            active=colors[2],
            inactive=colors[7],
            rounded=False,
            highlight_color=colors[1],
            highlight_method="line",
            this_current_screen_border="#e78a4e",
            this_screen_border=colors[4],
            other_current_screen_border=colors[6],
            other_screen_border=colors[4],
            foreground=colors[2],
            background=colors[0],
        ),
        widget.Sep(
            linewidth=0,
            padding=40,
            foreground=colors[2],
            background=colors[0]
        ),
        widget.WindowName(
            font="Ubuntu Mono",
            fontsize=14,
            foreground=colors[6],
            background=colors[0],
            padding=0
        ),

        widget.Systray(
            background=colors[0],
            padding=5,
        ),

        widget.Sep(
            linewidth=0,
            padding=10,
            foreground=colors[0],
            background=colors[0]
        ),

        widget.TextBox(
            font="Font Awesome 5 Free Solid",
            text='',
            foreground=colors[5],
            background="#111313",
            padding=-1,
            fontsize=50
        ),

        widget.Sep(
            linewidth=0,
            padding=1,
            foreground=colors[2],
            background=colors[5]
        ),
        widget.TextBox(
            font="Font Awesome 5 Free Solid",
            text='',
            # text='',
            # text='',
            foreground=colors[2],
            background=colors[5],
            padding=2,
            fontsize=14
        ),

        widget.Battery(
            battery="BAT0",
            discharge_char="",
            charge_char="",
            full_char="",
            unknown_char="?",
            format='{percent:2.0%} {char}',
            foreground=colors[2],
            background=colors[5],
            padding=5,
            update_interval=1,
            fontsize=12
        ),


        widget.TextBox(
            text="",
            foreground=colors[2],
            background=colors[5],
            fontsize=14,
            padding=0

        ),

        widget.Backlight(

            backlight_name="intel_backlight",
            foreground=colors[2],
            background=colors[5],
            fontsize=12,
            padding=5
        ),


        widget.TextBox(
            # text="",
            text="",
            foreground=colors[2],
            background=colors[5],
            fontsize=14,
            padding=0

        ),

        widget.CurrentLayout(

            foreground=colors[2],
            background=colors[5],
            padding=5
        ),


        widget.TextBox(
            text="",
            foreground=colors[2],
            background=colors[5],
            padding=0,
            fontsize=14
        ),
        widget.Memory(
            foreground=colors[2],
            background=colors[5],
            mouse_callbacks={
                'Button1': lambda: qtile.cmd_spawn('urxvt -e htop')},
            padding=5
        ),

        widget.TextBox(
            # text="",

            text="",
            foreground=colors[2],
            background=colors[5],
            fontsize=14,
            padding=0,
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("pavucontrol")}

        ),
        widget.Volume(
            foreground=colors[2],
            background=colors[5],
            padding=5
        ),


        widget.CheckUpdates(
            update_interval=1800,
            distro="Arch",
            display_format="  {updates} Updates",
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(
                terminal + ' -e yay -Suy')},
            background=colors[5],
            colour_have_updates=colors[2],
            padding=5,
            fontsize=14

        ),

        widget.TextBox(
            text="",
            foreground=colors[2],
            background=colors[5],
            fontsize=14,
            padding=0,
        ),


        widget.Clock(

            #font="Ubuntu Bold",
            # foreground="#1d2021",
            # background="#a9b665", # green
            # background="#ea6962", # red

            # foreground="#c6b392",
            #          background="#504945",
            mouse_callbacks={

                # For full hd 1920x1080
                'Button1': lambda: qtile.cmd_spawn("yad  --geometry=+1630+0  --calendar")},

            # For non standard half hd - 1366x768
            # 'Button1': lambda: qtile.cmd_spawn("yad  --geometry=+1100+0  --calendar")},


            foreground=colors[2],
            background=colors[5],

            padding=5,
            format="%A, %B %d - %H:%M "
        ),
    ]
    return widgets_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()

    # For multi monitor only
    # Slicing removes unwanted widgets (systray) on Monitors 1,3
   # del widgets_screen1[7:8] # Removing this as this is my monitor
    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    # Monitor 2 will display all widgets in widgets_list
    return widgets_screen2


# Single monitor support only
def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=0.95, size=25))]


if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(title='kdenlive'),  # GPG key password entry
    Match(title='SpeedCrunch'),  # GPG key password entry

    Match(wm_class='yad'),  # yad for calendar

    Match(wm_class='connman-gtk'),  # connman-gtk

    Match(wm_class='xdman-Main'),  # xdm floating solved

],
    border_focus="#1d2021"
)
auto_fullscreen = True
focus_on_window_activation = "smart"


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])


@hook.subscribe.client_new
def client_new(client):
    if 'qBittorrent' in client.name:
        client.togroup('TOR')
    elif "VirtualBox" in client.name:
        client.togroup('VBX')
    elif "Cantata" in client.name:
        client.togroup('MUS')
    elif "Brave" in client.name:
        client.togroup('WEB')
    else:
        logger.warning("ELSE " + client.name)


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
