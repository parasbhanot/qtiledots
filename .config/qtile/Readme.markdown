# HI
## H2
### H3
#### H4
##### H6
###### H7
####### H8

parasgraph
<br> 

Style

**bold text**
*italic*
***bold + italic***

> Block quote

> This is muliline
>
> Block quote


> Dorothy followed her through many of the beautiful rooms in her castle.
>
>> This is nested Block  quote


> #### complicated block quote
>
> - Revenue was off the chart.
> - Profits were higher than ever.
>
>  *Everything* is going according to **plan**.


1. First item
2. Second item
3. Third item
    1. Indented item
    2. Indented item
4. Fourth item

- First item
- Second item
+ First item
* first item
* second item

*   This is the first list item.
*   Here's the second list item.

    > A blockquote would look great below the second list item.

*   And here's the third list item.


--- 

My favorite search engine is [Duck Duck Go](https://duckduckgo.com).
<br>

I love supporting the **[EFF](https://eff.org)**.

This is the *[Markdown Guide](https://www.markdownguide.org)*.

See the section on [`code`](#code).
