#!/usr/bin/sh

#EDITOR = "nvim" 

declare -a options=(
"fish"
"dxhd"
"espanso"
"autostart"
"qtile"
)
#choice=$(printf '%s\n' "${options[@]}" | sort | dmenu -fn 'terminus-14' -i -l 20 -p "Select")

choice=$(printf '%s\n' "${options[@]}" | sort | rofi -theme parasSidebar -dmenu -p " ....")

case $choice in
        "fish") kitty -e nvim ~/.config/omf/init.fish ;;
        "dxhd") kitty -e nvim ~/.config/dxhd/dxhd.sh ;;
        "espanso")kitty -e nvim ~/.config/espanso/default.yml ;;
        "autostart")kitty  nvim ~/.config/qtile/autostart.sh ;;
        "qtile") kitty -e nvim  ~/.config/qtile/config.py ;;
        *) echo "invalid operation" && exit ;;
esac

