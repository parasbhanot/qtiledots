starship init fish | source
#neofetch
#source $HOME/.cargo/env
set -g man_blink -o blue
set -g man_bold -o red
set -g man_standout -b black 93a1a1
set -g man_underline -u green

alias ls="lsd"
alias lsl="exa -al --group-directories-first"

function fish_user_key_bindings
	fzf_key_bindings
end

#set -x EDITOR nvim
export EDITOR=nvim

alias bat="bat --theme gruvbox"
#alias yay="paru"

colorscript random

alias svi="sudoedit"
alias vi="nvim"

#yadm aliases 
alias yg="yadm"
alias yga="yadm add"
alias ygp="yadm push -u origin master"
alias ygs="yadm status"
alias ygl="yadm log"
alias ygd="yadm diff"
alias ygc="yadm checkout"
#set fish_greeting "hello parasbhanot >>> $TERM"
set fish_greeting "hello parasbhanot.."

alias rn="ranger"
alias ydl="youtube-dl"

#export LANG=en_IN.UTF-8
